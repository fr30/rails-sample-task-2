class UsersController < ApplicationController
  before_action :set_user, only: [:update, :show]

  def index
    @users = User.all
  end

  def show
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to users_url, notice: 'User has been saved'
    else
      render 'new'
    end
  end

  private

  def set_user
      @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:first_name,
                                 :last_name,
                                 :email_address,
                                 :phone_number,
                                 :date_of_birth,
                                 :user_address_attributes => [
                                     :street,
                                     :city,
                                     :zip_code,
                                     :country],
                                 :company_attributes => [
                                     :name,
                                     :company_address_attributes => [
                                         :street,
                                         :city,
                                         :zip_code,
                                         :country]])
  end
end
