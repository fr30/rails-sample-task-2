class Company < ApplicationRecord
  has_one :company_address, dependent: :destroy
  accepts_nested_attributes_for :company_address
  belongs_to :user
  validates :name, length: { maximum: 200 }
end
