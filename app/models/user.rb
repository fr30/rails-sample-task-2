class User < ApplicationRecord
  has_one :user_address, dependent: :destroy
  accepts_nested_attributes_for :user_address
  has_one :company, dependent: :destroy
  accepts_nested_attributes_for :company
  validates :first_name, presence: true, length: { maximum: 100 }
  validates :last_name, presence: true, length: { maximum: 100 }
  validates :email_address, email: { disposable: true, message: "is invalid" }
  validates :date_of_birth, datetime: { before: :today, before_message: "is in the future", message: "is invalid" }
  validates :phone_number, phone: { allow_blank: true }
end
