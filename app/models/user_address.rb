class UserAddress < ApplicationRecord
  belongs_to :user
  validates :street, presence: true
  validates :city, presence: true
  validate :zipcode
  validate :country_is_correct

  private

  def zipcode
    if(country_to_alpha2.nil?)
      return false
    end
    if !ValidatesZipcode::Zipcode.new({ zipcode: zip_code, country_alpha2: country_to_alpha2 }).valid?
      errors.add(:zip_code, "is invalid")
      return false
    end
    return true
  end

  def country_to_alpha2
    country_object = ISO3166::Country.find_country_by_name(country)
    if country_object.nil?
      return nil
    else
      country_object.alpha2
    end
  end

  def country_is_correct
    if country.nil? or country == ""
      errors.add(:country, "can't be blank")
      return false
    end
    if ISO3166::Country.find_country_by_name(country).nil?
      errors.add(:country, 'is incorrect')
      return false
    end
  end
end
