class CompanyAddress < ApplicationRecord
  belongs_to :company
  validate :country_is_correct
  validate :zipcode

  private

  def zipcode
    if(zip_code.nil? or zip_code == "")
      return true
    end
    if(country_to_alpha2.nil?)
      errors.add(:zip_code, "is invalid")
      return false
    end
    if !ValidatesZipcode::Zipcode.new({ zipcode: zip_code, country_alpha2: country_to_alpha2 }).valid?
      errors.add(:zip_code, "is invalid")
      return false
    end
  end

  def country_to_alpha2
    country_object = ISO3166::Country.find_country_by_name(country)
    if country_object.nil?
      return nil
    else
      country_object.alpha2
    end
  end

  def country_is_correct
    if country.nil? or country == ""
      return true
    end
    if ISO3166::Country.find_country_by_name(country).nil?
      errors.add(:country, 'is incorrect')
      return false
    end
  end
end
