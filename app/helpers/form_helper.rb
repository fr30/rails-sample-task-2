module FormHelper
  def setup_user(user)
    user.user_address ||= UserAddress.new
    user.company ||= Company.new
    user.company.company_address = CompanyAddress.new
    user
  end
end