The app allows you to create users using sophisticated form. Users can only be created and displayed (you can't delete or edit them).

Before starting the app make sure you have all required gems installed. You can do it by typing command 

bundle install

In order to start the app, you have to type standard commands for starting ruby server and generate a database, namely:

rake db:setup


rails server

List of available endpoints can be obtained by:

rails routes

However, from root path it's pretty intuitive how to move around the site.

There are 4 tables in database: Users, UserAddresses, Companies, CompanyAddreses. The reason for 2 different models for addresses is different rules for validation. There are three 1-1 relationships in database: Users-Companies (single user can have a single company, every company has to belong to a user), Users-UserAddresses (single user can have a single user address, a single user address has to belong to a user), Companies-CompanyAddresses (single company can have a single company address, a single company address has to belong to a company). 

I've written a few unit tests for models using ActiveRecord tests (inherits from minitest) and you can find them in directory /test. There are no integration tests.

