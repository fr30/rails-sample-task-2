class CreateUserAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :user_addresses do |t|
      t.string :street
      t.string :city
      t.string :zip_code
      t.string :country
      t.references :user
      t.timestamps
    end
  end
end
