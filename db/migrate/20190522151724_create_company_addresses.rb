class CreateCompanyAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :company_addresses do |t|
      t.string :street
      t.string :city
      t.string :zip_code
      t.string :country
      t.references :company
      t.timestamps
    end
  end
end
