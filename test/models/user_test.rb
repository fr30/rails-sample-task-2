require 'test_helper'

class UserTest < ActiveSupport::TestCase
  setup do
    @user = User.new(first_name: "Jack",
                     last_name: "Gordon",
                     email_address: "random@email.com",
                     date_of_birth: "01/03/1999")
  end

  test "should save the user" do
    assert_nothing_raised{ @user.save! }
  end

  test "shouldn't save the user without a first name" do
    @user.first_name = nil
    exception = assert_raise(Exception) { @user.save! }
    assert_equal( "Validation failed: First name can't be blank", exception.message)
  end

  test "shouldn't save the user with a first name longer than 100 characters" do
    invalid_first_name = ''
    for i in (1..101)
      invalid_first_name += 'a'
    end
    @user.first_name = invalid_first_name
    exception = assert_raise(Exception) { @user.save! }

    assert_equal( "Validation failed: First name is too long (maximum is 100 characters)", exception.message)
  end

  test "shouldn't save the user without a last name" do
    @user.last_name = nil
    exception = assert_raise(Exception) { @user.save! }
    assert_equal( "Validation failed: Last name can't be blank", exception.message)
  end

  test "shouldn't save the user with a last name longer than 100 characters" do
    invalid_last_name = ''
    for i in (1..101)
      invalid_last_name += 'a'
    end
    @user.last_name = invalid_last_name
    exception = assert_raise(Exception) { @user.save! }

    assert_equal( "Validation failed: Last name is too long (maximum is 100 characters)", exception.message)
  end

  test "shouldn't save the user without a email address" do
    @user.email_address = nil
    exception = assert_raise(Exception) { @user.save! }
    assert_equal( "Validation failed: Email address is invalid", exception.message)
  end

  test "shouldn't save the user with invalid email address" do
    @user.email_address = "abcdfasda"
    exception = assert_raise(Exception) { @user.save! }
    assert_equal( "Validation failed: Email address is invalid", exception.message)
  end

  test "shouldn't save the user without date of birth" do
    @user.date_of_birth = nil
    exception = assert_raise(Exception) { @user.save! }
    assert_equal( "Validation failed: Date of birth is invalid", exception.message)
  end

  test "shouldn't save the user with invalid date of birth" do
    @user.date_of_birth = "31/02/1999"
    exception = assert_raise(Exception) { @user.save! }
    assert_equal( "Validation failed: Date of birth is invalid", exception.message)
  end

  test "shouldn't save the user with date of birth in future" do
    @user.date_of_birth = "01/03/2100"
    exception = assert_raise(Exception) { @user.save! }
    assert_equal( "Validation failed: Date of birth is in the future", exception.message)
  end

end
