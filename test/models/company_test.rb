require 'test_helper'

class CompanyTest < ActiveSupport::TestCase
  setup do
    @user = User.new(first_name: "Jack",
                     last_name: "Gordon",
                     email_address: "random@email.com",
                     date_of_birth: "01/03/1999")
    @user.save!
    @company = Company.new(user: @user)
  end

  test "shouldn't save the company with a name longer than 200 characters" do
    invalid_name = ''
    for i in (1..201)
      invalid_name += 'a'
    end
    @company.name = invalid_name
    exception = assert_raise(Exception) { @company.save! }

    assert_equal( "Validation failed: Name is too long (maximum is 200 characters)", exception.message)
  end

  test "should save blank company" do
    assert_nothing_raised { @company.save! }
  end
end
