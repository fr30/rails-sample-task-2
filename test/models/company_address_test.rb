require 'test_helper'

class CompanyAddressTest < ActiveSupport::TestCase
  setup do
    @user = User.new(first_name: "Jack",
                     last_name: "Gordon",
                     email_address: "random@email.com",
                     date_of_birth: "01/03/1999")
    @user.save!
    @company = Company.new(user: @user)
    @company.save!
    @company_address = CompanyAddress.new(street: "Sesame st.",
                                          city: "Wadowice",
                                          zip_code: "21-370",
                                          country: "Poland",
                                          company_id: @company.id)
  end

  test "shouldn't save with blank zip code" do
    @company_address.zip_code = nil
    exception = assert_nothing_raised { @company_address.save! }
  end

  test "shouldn't save with incorrect zip code" do
    @company_address.zip_code = "1234"
    exception = assert_raise(Exception) { @company_address.save! }
    assert_equal("Validation failed: Zip code is invalid", exception.message )
  end

  test "should save correct company address" do
    assert_nothing_raised { @company_address.save! }
  end

  test "shouldn't save with incorrect country" do
    @company_address.country = "asdfsdakfjasljlas"
    exception = assert_raise(Exception) { @company_address.save! }
    assert_equal("Validation failed: Country is incorrect", exception.message )
  end

end
