require 'test_helper'
#TODO add zip_code validation tests
class UserAddressTest < ActiveSupport::TestCase
  setup do
    @user = User.new(first_name: "Jack",
                     last_name: "Gordon",
                     email_address: "random@email.com",
                     date_of_birth: "01/03/1999")
    @user.save!
    @user_address = UserAddress.new(street: "Sesame st.",
                                    city: "Wadowice",
                                    zip_code: "34-100",
                                    country: "Poland",
                                    user: @user)
  end

  test "should save correct user address" do
    assert_nothing_raised { @user_address.save! }
  end

  test "shouldn't save with blank zip code" do
    @user_address.zip_code = nil
    exception = assert_raise(Exception) { @user_address.save! }
    assert_equal("Validation failed: Zip code is invalid", exception.message )
  end

  test "shouldn't save with incorrect zip code" do
    @user_address.zip_code = "1234"
    exception = assert_raise(Exception) { @user_address.save! }
    assert_equal("Validation failed: Zip code is invalid", exception.message )
  end

  test "shouldn't save with blank street" do
    @user_address.street = nil
    exception = assert_raise(Exception) { @user_address.save! }
    assert_equal("Validation failed: Street can't be blank", exception.message )
  end

  test "shouldn't save with blank city" do
    @user_address.city = nil
    exception = assert_raise(Exception) { @user_address.save! }
    assert_equal("Validation failed: City can't be blank", exception.message )
  end

  test "shouldn't save with blank country" do
    @user_address.country = nil
    exception = assert_raise(Exception) { @user_address.save! }
    assert_equal("Validation failed: Country can't be blank", exception.message )
  end

  test "shouldn't save with incorrect country" do
    @user_address.country = "asdfsdakfjasljlas"
    exception = assert_raise(Exception) { @user_address.save! }
    assert_equal("Validation failed: Country is incorrect", exception.message )
  end

end
