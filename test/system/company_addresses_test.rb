require "application_system_test_case"

class CompanyAddressesTest < ApplicationSystemTestCase
  setup do
    @company_address = company_addresses(:one)
  end

  test "visiting the index" do
    visit company_addresses_url
    assert_selector "h1", text: "Company Addresses"
  end

  test "creating a Company address" do
    visit company_addresses_url
    click_on "New Company Address"

    click_on "Create Company address"

    assert_text "Company address was successfully created"
    click_on "Back"
  end

  test "updating a Company address" do
    visit company_addresses_url
    click_on "Edit", match: :first

    click_on "Update Company address"

    assert_text "Company address was successfully updated"
    click_on "Back"
  end

  test "destroying a Company address" do
    visit company_addresses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Company address was successfully destroyed"
  end
end
