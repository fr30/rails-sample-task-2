require 'test_helper'

class CompanyAddressesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @company_address = company_addresses(:one)
  end

  test "should get index" do
    get company_addresses_url
    assert_response :success
  end

  test "should get new" do
    get new_company_address_url
    assert_response :success
  end

  test "should create company_address" do
    assert_difference('CompanyAddress.count') do
      post company_addresses_url, params: { company_address: {  } }
    end

    assert_redirected_to company_address_url(CompanyAddress.last)
  end

  test "should show company_address" do
    get company_address_url(@company_address)
    assert_response :success
  end

  test "should get edit" do
    get edit_company_address_url(@company_address)
    assert_response :success
  end

  test "should update company_address" do
    patch company_address_url(@company_address), params: { company_address: {  } }
    assert_redirected_to company_address_url(@company_address)
  end

  test "should destroy company_address" do
    assert_difference('CompanyAddress.count', -1) do
      delete company_address_url(@company_address)
    end

    assert_redirected_to company_addresses_url
  end
end
